function [error_train, error_val] = ...
    learningCurve(X_train,X_val,y_train,y_val,lambda)
% compute the learning curves i.e. how well does the prediction over the
% validation set improve as we give more and more data to optimize beta

[m,n] = size(X_train);
m_val = size(y_val);
error_train = zeros(m, 1);
error_val   = zeros(m, 1);
for i=1:m
    % search for best beta
    all_beta = oneVsAll(X_train(1:i,:), y_train(1:i,:), lambda);
    
    % cost over restricted-training set
    J_train = lrCostFunction(all_beta(1,:)', X_train(1:i,:), y_train(1:i,:), 0);
    
    % cost over the cross validation set
    J_val = lrCostFunction(all_beta(1,:)', X_val, y_val, 0);
    
    error_train(i) = J_train;
    error_val(i) = J_val;
end

figure()
hold all;
plot([1:m]', error_train, 'b');
plot([1:m]', error_val, 'g');
hold off;
legend('training error', 'validation error');
xlabel('training index');
ylabel('error');
title('learning curve');

end

