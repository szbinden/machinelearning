function [] = submit(filename, all_beta, model, mu, sigma, k)
%% load validation data set

load('trainedClassifier');

data = csvread('validate_and_test.csv');
index = data(:,1);

X = data(:,2:8); % remove first


[m,n] = size(X); % m: number of training data, n: number of features

% normalize:
X_norm = bsxfun(@minus, X, mu);
X_norm = bsxfun(@rdivide, X, sigma);

% PCA:
% X_PCA = PrincipalComponentAnalysis(k, X_norm);

% Add Ones:
% X_PCA = [ones(size(X_norm, 1), 1), X_PCA]; 

%% predict with all_beta

% [y, ~] = maximizeHypothesis(all_beta, X_PCA);

%% predict with model

y = predict(model, X_norm);

%% predict with Classifier

% y = predict(trainedClassifier.ClassificationSVM, X);

%% create csv file to export/submit our solution to kaggle

% copy-paste of the project description PDF
label = y;
ids = index;

res = horzcat(ids, label);

table = array2table(res, 'VariableNames', {'Id', 'Label'});
writetable(table, filename);

end

