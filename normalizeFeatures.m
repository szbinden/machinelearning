function [X_norm, mu, sigma] = normalizeFeatures( X )
% this function normalize all the column vectors of the matrix X but the
% first one which is only made of ones.

mu = mean(X);
X_norm = bsxfun(@minus, X, mu);

sigma = std(X_norm);
X_norm = bsxfun(@rdivide, X_norm, sigma);

end

