function [J, grad] = lrCostFunction(beta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(beta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
[m, n] = size(X); % number of training examples

J = 0;
temp = beta;
temp(1) = 0; % remove beta0 => we do not regularized beta0

grad = zeros(size(beta));

for i = 1:m
    
    hx = hypothesis(beta, X(i,:));
    J = J + y(i)*log(hx) - (1 - y(i))*log(1 - hx);
    
end

% Regularization:
    
J = J + lambda*sum(temp.^2);

% Normalization:
J = -J/m;

grad = X'*(X*beta - y) + lambda*temp;
grad = grad/m;

grad = grad(:);

end