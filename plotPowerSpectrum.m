function [ output_args ] = plotPowerSpectrum()
% Standalone function to be used directly from the matlab command window.
% It plots the power spectrum (magnitude of the fourier tranform) again the
% signal frequency (in Hz not rad)

% IMPORTANT: plots of y0, y1, y2 are shown side by side although they are
% absolutely no related !!!


clc; close all; clear all;

data = csvread('train.csv');                % load the training data (n features)

y = data(:,9);                              % last vector is the measured results (sleep state label)
X = data(:,2:8);                            % remove first and last column
[m,n] = size(X);                            % m: number of training data, n number of features

fb = [0.49;5;9;15;23;32;64];

fb_mean = (fb(1:end-1) + fb(2:end))/2;

y0 = find(y == 0);
y1 = find(y == 1);
y2 = find(y == 2);

fprintf('Please remember that the 3 plots are absolutely uncorrelated !\n\n')

XMIN = min(fb_mean);
XMAX = max(fb_mean);
YMAX = max(max(X(y0,1:end-1)));
for i=1:min([length(y0), length(y1), length(y2)])
    figure(1)
    
    subplot(3,1,1)
    plot(fb_mean, X(y0(i),1:end-1),'b')
    hold on
    plot(fb_mean, X(y0(i),1:end-1),'bo')
    hold off
    xlim([XMIN XMAX])
    ylim([0 YMAX])
    legend('0 : wake','features 1 to 6')
    title('Power spectrum')
    
    subplot(3,1,2)
    plot(fb_mean, X(y1(i),1:end-1),'r')
    hold on
    plot(fb_mean, X(y1(i),1:end-1),'ro')
    hold off
    xlim([XMIN XMAX])
    ylim([0 YMAX])
    legend('1 : REM','features 1 to 6')
    
    subplot(3,1,3)
    plot(fb_mean, X(y2(i),1:end-1),'k')
    hold on
    plot(fb_mean, X(y2(i),1:end-1),'ko')
    hold off
    xlim([XMIN XMAX])
    ylim([0 YMAX])
    legend('2 : NREM','features 1 to 6')
    
    xlabel('frequenz Hz')
    
    input('press enter')
    
    clc;
end
end

