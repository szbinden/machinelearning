function [] = plotFeatureVsFeature(X,y)
%% Scatter plots of every combination of two features versus their y value.


[m,n] = size(X); 

i0 = find(y == 0);
i1 = find(y == 1);
i2 = find(y == 2);

index = 1;
figure()
for i=1:n-1
    for j=i+1:n
            subplot(5,6,index)
            scatter(X(i0,i),X(i0,j),'r');
            hold on
            scatter(X(i1,i),X(i1,j),'b');
            scatter(X(i2,i),X(i2,j),'g');
            % title(strcat('feature',num2str(i), 'and ', num2str(j),' vs. y'))
            hold off
            index=index+1;
    end
end


end

