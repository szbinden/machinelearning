%% Machine Learning Porject 1

% Team: Sofia Zbinden, Édouard Gence and Joran Fonjallaz

%% Initialization
clc; clear all; close all;

load('trainedClassifier.mat');

%% Load training data and define variables

data = csvread('train.csv'); % load the training data (n features)

y = data(:,9); % last vector is the measured results (sleep state label)
X = data(:,2:8); % remove first and last column
[m,n] = size(X); % m: number of training data, n number of features

%% plot data to get intuition (or not...)

% plotFeatures(X,y);
 
% plotFeatureVsFeature(X,y);

%% Extract Features

% Extract feature 3 & 6:
% X = [X(:,1:2), X(:,4:5), X(:,7)];

%% normalize features

[X_norm, mu, sigma] = normalizeFeatures(X);

%% Principal Component Analysis

% Reduce Feature Dimension n = 7 to k:
k = 1;
% k = searchDimensionK(X_norm)
% X_PCA = PrincipalComponentAnalysis(k, X_norm);

%% define our training, validation and test sets

m_train = round(m*0.6); % we alocate 60% of our data set to the training set
m_val = round(m*0.2); % 20% of the data set goes for the validation set
m_test = m - m_train - m_val; % the last 20% are used the the test set

% split X into 3 data sets i.e. training, validation and test set
X_train = X_norm(1:m_train,:);
y_train = y(1:m_train);
X_val = X_norm(m_train+1:m_train+m_val,:);
y_val = y(m_train+1:m_train+m_val);
X_test = X_norm(m_train+m_val+1:m_train+m_val+m_test,:);
y_test = y(m_train+m_val+1:m_train+m_val+m_test);

%% Add Ones:

% X_train = [ones(size(X_train,1), 1), X_train];
% X_val = [ones(size(X_val, 1), 1), X_val];
% X_test = [ones(size(X_test, 1), 1), X_test]; % add Ones

%% logistic regression One-vs-All (search for best beta)

lambda = 0.1; % regularization parameter (set to zero for unregularized cost)
% 
[all_beta] = zeros(3, n+1);
% [all_beta] = oneVsAll(X_train, y_train, lambda);
% 
% % J_train = lrCostMaximized(all_beta, X_train, y_train, lambda)
% % J_val = lrCostMaximized(all_beta, X_val, y_val, lambda)
% % J_test = lrCostMaximized(all_beta, X_test, y_test, lambda)
% 
% J_train = zeros(3,1);
% J_val = zeros(3,1);
% J_test = zeros(3,1);
% for c = 1:3
%     [J_train(c), ~] = lrCostFunction(all_beta(c,:)', X_train, y_train == (c-1), 0);
%     [J_val(c), ~] = lrCostFunction(all_beta(c,:)', X_val, y_val == (c-1), 0);
%     [J_test(c), ~] = lrCostFunction(all_beta(c,:)', X_test, y_test == (c-1), 0);
% end
% J_train
% J_val
% J_test

%% support vector machine 

% NaiveBayesModel = fitNaiveBayes(X_train,y_train);
% [post_train,p_train] = posterior(NaiveBayesModel,X_train);
% [post_val,p_val] = posterior(NaiveBayesModel,X_val);
% [post_test,p_test] = posterior(NaiveBayesModel,X_test);
% accuracy = sum((y_train == p_train)/m_train)
% accuracy = sum((y_val == p_val)/m_val)
% accuracy = sum((y_test == p_test)/m_test)
% loss = loss(NaiveBayesModel,X_val,y_val)
% loss = loss(SVMModel,X_test,y_test)

%% Gaussian Mixture Model

options = statset('MaxIter',1000);
Model_f1 = fitgmdist(X_train(:,1), 3, 'Options', options);
Model_f2 = fitgmdist(X_train(:,2), 3, 'Options', options);
% threshold = 

p_f1 = posterior(Model_f1, X_train(:,1))
p_f2 = posterior(Model_f2, X_train(:,2))

% Cluster_train = cluster(Model,X_train);
% Cluster_train = bestMap(y_train, Cluster_train);
% Cluster_val = cluster(Model,X_val);
% Cluster_val = bestMap(y_val, Cluster_val);
% 
% accuracy = sum(y_train==Cluster_train)/m_train
% accuracy = sum(y_val==Cluster_val)/m_val

%% K-means

% idx = kmeans(X_train, 3);
% AC = bestMap(y_train, idx)
% 
% accuracy = sum((y_train == AC))/m_train

%% trained Classifier with classificationLearner

% J_train = loss(trainedClassifier.ClassificationSVM, X_train, y_train)
% J_val = loss(trainedClassifier.ClassificationSVM, X_val, y_val)
% J_test = loss(trainedClassifier.ClassificationSVM, X_test, y_test)


%% Learning Curves

% [err_train1, err_val1] = learningCurve(X_train, X_val, y_train, y_val, lambda);

%% do prediction on unknown data set and create csv for kaggle submition

% submit('prediction.csv', all_beta, NaiveBayesModel, idx, mu, sigma, k);
