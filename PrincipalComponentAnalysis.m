function X_PCA = PrincipalComponentAnalysis(k, X)
% Principal Component Analysis of feature Vectors X to reduce the
% dimensions of features from n to k.

[m,n] = size(X);

% covariance matrix:
Sigma = (1/m) * X' * X;
% Eigenvectors of Sigma:
[U,~,~] = svd(Sigma);
% Reduce U to k vectors:
Ureduced = U(:,1:k);
X_PCA = X * Ureduced;

end