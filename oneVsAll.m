function [all_beta] = oneVsAll(X, y, lambda)
%ONEVSALL trains multiple logistic regression classifiers and returns all
%the classifiers in a matrix all_beta, where the i-th row of all_beta 
%corresponds to the classifier for label i
%   [all_beta] = ONEVSALL(X, y, lambda) trains 3
%   logisitc regression classifiers and returns each of these classifiers
%   in a matrix all_beta, where the i-th row of all_beta corresponds 
%   to the classifier for label i

% Some useful variables
[m,n] = size(X);

% You need to return the following variables correctly 
all_beta = zeros(3, n);

% Set options for fminunc
options = optimset('GradObj', 'on', 'MaxIter', 100, 'PlotFcns', @optimplotfv);

for c = 1:3
    
    y_c = y == (c-1);
    initial_beta = all_beta(c,:)';
    costFunction = @(t) lrCostFunction(t, X, y_c, lambda);
    beta = fmincg(costFunction, initial_beta, options);
    all_beta(c,:) = beta';
    
end
