function Cost = lrCostMaximized(all_beta, X, y, lambda)
% Function to find the Cost from the predicted y with maximizeHypothesis
% without regularization

[m,n] = size(X);

[~, yProbability] = maximizeHypothesis(all_beta, X);

Cost = 0;

for i = 1:m
    
    Cost = Cost + y(i)*log(yProbability(i)) - (1 - y(i))*log(1 - yProbability(i));
    
end

% Normalization:
Cost = -Cost/m;

end