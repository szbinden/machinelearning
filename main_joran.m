clc; close all; clear all;

data = csvread('train.csv');                % load the training data (n features)

y = data(:,9);                              % last vector is the measured results (sleep state label)
X = data(:,2:8);                            % remove first and last column
[m,n] = size(X);                            % m: number of training data, n number of features

