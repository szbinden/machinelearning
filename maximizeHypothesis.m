function [yPredict, yProbability] = maximizeHypothesis(all_beta, X)
% Function to find for every index i the maximal hypothesis: predict y with
% the maximal hypothesis and store the chosen beta(i).

[m,n] = size(X);

y_predict = zeros(m,1);
% matrix with predictions for all beta:
y_predict = [hypothesis(all_beta(1,:)',X),... 
    hypothesis(all_beta(2,:)',X),...
    hypothesis(all_beta(3,:)',X)];
% Take maximal index value for every test data:
[yProbability,I] = max(y_predict');
yProbability = yProbability';
yPredict = I' - 1;

end