function k = searchDimensionK(X)
% Function to search for the dimension k of the Principal Component
% Analysis. Retain 99% of variance.

[m,n] = size(X);

% covariance matrix:
Sigma = (1/m) * X' * X;
% Eigenvectors of Sigma:
[~,S,~] = svd(Sigma);
% Search best dimension k:
for k = 1:n
    
    Test = trace(S(1:k,1:k))/trace(S);
    
    if (Test >= 0.99)
        k = k;
        break;
    end
    
end

end