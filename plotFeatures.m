function [] = plotFeatures(X,y)
%% Plot of every feature versus his y value.

[m,n] = size(X);

figure()
for i=1:n
    subplot(4,4,i)
    plot(X(:,i), y, 'bx');
    hold on
    title(strcat('feature',num2str(i-1),' vs. y'))
end


end

