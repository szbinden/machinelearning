function h = hypothesis(beta, X)
%% hypothesis function for logistic regression (Sigmoid function)
% Estimates the probability that y = 1 on input x parameterized by beta.
% h(x) = p(y=1|x;beta)
% Decision Boundary: y = 1 if h >= 0.5 if x*beta >= 0
%                    y = 0 if h < 0.5 if x*beta < 0

z = X*beta;
h = 1.0 ./ (1.0 + exp(-z));

end